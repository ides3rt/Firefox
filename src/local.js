//-----------------------------------------------------------------------------
// Description: Additional configuration for Arkenfox's User.JS
// Author: iDes3rt <ides3rt@protonmail.com>
// Source: https://codeberg.org/ides3rt/Firefox
// Last Modified: 2023-07-12
//-----------------------------------------------------------------------------

user_pref("apz.frame_delay.enabled", false);
user_pref("browser.download.autohideButton", false);
user_pref("browser.download.start_downloads_in_tmp_dir", true);
user_pref("browser.menu.showViewImageInfo", true);
user_pref("browser.preferences.moreFromMozilla", false);
user_pref("browser.quitShortcut.disabled", true);
user_pref("browser.sessionstore.idleDelay", 60000);
user_pref("browser.sessionstore.interval", 300000);
user_pref("browser.shell.checkDefaultBrowser", false);
user_pref("browser.tabs.warnOnCloseOtherTabs", false);
user_pref("browser.tabs.warnOnOpen", false);
user_pref("browser.toolbars.bookmarks.visibility", "always");
user_pref("browser.warnOnQuitShortcut", false);
user_pref("extensions.pocket.enabled", false);
user_pref("full-screen-api.warning.delay", 0);
user_pref("full-screen-api.warning.timeout", 0);
user_pref("general.autoScroll", true);
user_pref("general.smoothScroll", false);
user_pref("general.smoothScroll.currentVelocityWeighting", 1);
user_pref("general.smoothScroll.msdPhysics.continuousMotionMaxDeltaMS", 12);
user_pref("general.smoothScroll.msdPhysics.enabled", true);
user_pref("general.smoothScroll.msdPhysics.motionBeginSpringConstant", 200);
user_pref("general.smoothScroll.msdPhysics.regularSpringConstant", 250);
user_pref("general.smoothScroll.msdPhysics.slowdownMinDeltaMS", 25);
user_pref("general.smoothScroll.msdPhysics.slowdownMinDeltaRatio", 2);
user_pref("general.smoothScroll.msdPhysics.slowdownSpringConstant", 250);
user_pref("general.smoothScroll.stopDecelerationWeighting", 1);
user_pref("gfx.x11-egl.force-enabled", true);
user_pref("identity.fxaccounts.enabled", false);
user_pref("layout.css.visited_links_enabled", false);
user_pref("media.ffmpeg.vaapi.enabled", true);
user_pref("mousewheel.min_line_scroll_amount", 40);
user_pref("mousewheel.system_scroll_override.enabled", false);
user_pref("privacy.resistFingerprinting", false);
user_pref("privacy.resistFingerprinting.block_mozAddonManager", false);
user_pref("privacy.resistFingerprinting.letterboxing", false);
user_pref("signon.rememberSignons", false);
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
user_pref("ui.key.menuAccessKey", 0);
user_pref("widget.dmabuf.force-enabled", true);
