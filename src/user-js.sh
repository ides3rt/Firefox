#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: Download Arkenfox's User.JS and append it
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Firefox
# Last Modified: 2022-12-30
#------------------------------------------------------------------------------
# Dependencies:
#       aria2
#       bash
#       coreutils
#------------------------------------------------------------------------------

declare -r prg=${0##*/}

die ()
{
	echo "$prg: $2" >&2
	(( $1 )) &&
		exit $1
}

(( $# )) &&
	die 1 "needn't arguments."

declare -i no_bin=0
for bin in aria2c realpath; {
	if ! type -P "$bin" &>/dev/null; then
		die 0 "dependency, \`$bin\`, not met."
		(( no_bin++ ))
	fi
}

(( no_bin )) &&
	die 1 "$no_bin dependency(s) missing, aborted."
unset -v no_bin bin

tmp=$(realpath "$0")
src=${tmp%/*}/local.js
dest=${tmp%/*/*}/user.js
unset -v tmp

[[ -f $src ]] ||
	die 1 "$src: not found."

[[ -f $dest ]] &&
	die 1 "$dest: already existed."

aria2c -q -d "${dest%/*}" -o "${dest##*/}" \
	https://raw.githubusercontent.com/arkenfox/user.js/Thorin-Oakenpants-patch-1/user.js
	#https://raw.githubusercontent.com/arkenfox/user.js/master/user.js

(( $? )) &&
	die 1 'failed to retrieved the User.JS.'

read -d '' <<-EOF

// The following is created by '$prg', which is written by iDes3rt.

EOF
echo "$REPLY$(< "$src")" >> "$dest"

echo "$prg: $dest: the User.JS has been created."
